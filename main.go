package main

import (
  "os"
  "log"
  "fmt"
  "flag"
  "time"
  "bytes"
  "strconv"
  "strings"
  "net/url"
  "net/http"
  "io/ioutil"
  "encoding/json"
  "github.com/kurrik/oauth1a"
  "github.com/kurrik/twittergo"
)

func LoadCreds() (client *twittergo.Client, err error) {
  creds, err := ioutil.ReadFile("CREDENTIALS")
  if err != nil {
    panic(err)
  }
  log.Print("\033[38:2:150:150:0mCredentials file loaded.\033[0m")
  lines := strings.Split(string(creds), "\n")

  conf := &oauth1a.ClientConfig{
    ConsumerKey:  lines[0],
    ConsumerSecret:  lines[1],
  }
  usr := oauth1a.NewAuthorizedConfig(lines[2], lines[3])
  client = twittergo.NewClient(conf, usr)
  return client, err
}

type Arguments struct {
  ScreenN string
  OutputF string
}

func parseArguments() *Arguments {
  a := &Arguments{}
  flag.StringVar(&a.ScreenN, "screen_name", "twitterapi", "Screen name")
  flag.StringVar(&a.OutputF, "out", "output.json", "Output file")
  return a
}



func main() {
  if len(os.Args) < 2 || len(os.Args) > 2 {
    log.Print("\033[38:2:255:0:0mWRONG NUMBER OF ARGUMENTS PASSED\033[0m")
    log.Print("\033[38:2:0:255:0mUsage is `./retweeterang USERNAME`\033[0m")
    os.Exit(1)
  }
  var buffer bytes.Buffer
  log.New(&buffer, "retweeterang", log.Llongfile)
  log.Print("\033[38:2:150:150:0mLet's retweeterang!\033[0m")
  //Declaration block
  var (
    err  error
    client  *twittergo.Client
    req  *http.Request
    resp  *twittergo.APIResponse
    args  *Arguments
    max_id  uint64
    out  *os.File
    query  url.Values
    results  *twittergo.Timeline
    text  []byte
  )


  //Start consuming the variables
  args = parseArguments()
  client, err = LoadCreds()
  if err != nil {
    panic(err)
  }

  out, err = os.Create(args.OutputF)
  if err != nil {
    panic(err)
  }
  defer out.Close()

  //List the api endpoints needed
  const (
    count int = 200
    urllist  = "/1.1/statuses/user_timeline.json?%v"

    minWait = time.Duration(10) *time.Second
  )

  //Start the query setup
  query = url.Values{}
  query.Set("count", fmt.Sprintf("%v", count))
  query.Set("screen_name", os.Args[1])
  total := 0

  //Create an array to hold the tweet ids to unretweet
  var tweetIds []uint64

  //Start the main loop
  for {
    if max_id != 0 {
      query.Set("max_id", fmt.Sprintf("%v", max_id))
    }

    endpoint := fmt.Sprintf(urllist, query.Encode())
    req, err = http.NewRequest("GET", endpoint, nil)
    if err != nil {
      panic(err)
    }
    log.Print("\033[38:50:50:200mGetting a list of retweets.\033[0m")

    resp, err = client.SendRequest(req)
    if err != nil {
      panic(err)
    }
    log.Print("\033[38:150:150:0mRequest status "+resp.Status+"\033[0m")

    results = &twittergo.Timeline{}
    err = resp.Parse(results)
    if err != nil {
      panic(err)
    }
    log.Print("\033[38:50:50:200mParsing and unretweeting.\033[0m")
    if rle, ok := err.(twittergo.RateLimitError); ok {
      dur := rle.Reset.Sub(time.Now()) + time.Second
      if dur < minWait {
        dur = minWait
      }
      log.Print("\033[38:250:50:50mRate limited, waiting...\033[0m")
      time.Sleep(dur)
      continue
    } else {
      log.Print("\033[38:250:50:50mError parsing reponse.\033[0m")
    }

    batch := len(*results)
    if batch == 0 {
      log.Print("\033[38:150:150:0mNo more results, end of timeline.\033[0m")
      break
    }
    for _, tweet := range *results {
      text, err = json.Marshal(tweet)
      if err != nil {
        panic(err)
      }
      out.Write(text)
      out.Write([]byte("\n"))
      tweetIds = append(tweetIds, tweet.Id())
      max_id = tweet.Id() - 1
      total += 1
    }
    log.Print("\033[38:150:150:0mGot "+strconv.Itoa(batch)+" tweets.\033[0m")
  }
  for i := range tweetIds {
    //Now we unretweet the retweets
    urldestroy := fmt.Sprintf("/1.1/statuses/unretweet/%v.json?", tweetIds[i])
    endpoint := fmt.Sprintf(urldestroy)
    log.Print("\033[38:150:150:150mUn-Retweeting "+fmt.Sprint(tweetIds[i])+".\033[0m")
    req, err = http.NewRequest("POST", endpoint, nil)
    if err != nil {
      panic(err)
    }
    resp, err = client.SendRequest(req)
    if err != nil {
      panic(err)
    }
    results = &twittergo.Timeline{}
    time.Sleep(100*time.Millisecond)

  }




}
